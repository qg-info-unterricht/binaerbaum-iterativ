/**
 * Ein Stack basierend auf einer verketteten Liste
 * 
 * @author Rainer Helfrich
 * @author Frank Schiebel
 * @version Oktober 2020
 */
public class Stack<T> 
{
    
    //# TODO
    // Hier muessen Attribute und Konstruktor des Stacks festgelegt werden.
    // 
    // Zur Erinnerung:  
    // * "head" muss stest auf den Obersten Node des Stacks zeigen
    // * die Nodes sind vom Typ "SNode<T>" (schau nach was der Diamantoperator macht, wenn du 
    //   das nicht mehr weisst.
    // * Der Konstruktor muss einen leeren Stack erzeiugen - auf was zeigt dann "head"?
    //
    public Stack() {
        
    }
    
    /**
     * Gibt das oberste Element des Stacks zurück (falls der Stack nicht leer ist)
     * @return Das oberste Element
     */
    public T top()
    {
        //# TODO: Hier ist etwas zu tun!
        return null;
    }
    
    /**
     * Entfernt das oberste Element vom Stack (falls der Stack nicht leer ist) und gibt es zurück
     * @return Das bisherige oberste Element
     */
    public T pop()
    {
        //# TODO: Hier ist etwas zu tun!
        return null;
    }
    
    /**
     * Legt ein neues Element auf den Stack
     * @param x Das neue Element
     */
    public void push(T wert)
    {
        //# TODO: Hier ist etwas zu tun!
    }
    
    /**
     * Gibt zurück, ob der Stack leer ist
     * @return true, wenn der Stack leer ist; false sonst
     */
    public boolean isEmpty()
    {
        //# TODO: Hier ist etwas zu tun!
        return false;
    }
}
